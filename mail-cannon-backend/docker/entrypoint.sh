#!/bin/sh
echo 'Waiting for PostgreSQL to start...'

while ! nc -z "127.0.0.1" "54321"; do
	sleep 0.2
done

echo 'PostgreSQL started! Waiting 15 secs and migrating...'

sleep 15

python3 manage.py migrate --no-input
python3 manage.py collectstatic --no-input

exec "$@"

