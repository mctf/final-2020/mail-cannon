import logging
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP
from typing import List

from django.core.mail import EmailMessage
from django.core.mail.backends.base import BaseEmailBackend

log = logging.getLogger('mail-cannon.core.mailing')


class VolatileEmailBackend(BaseEmailBackend):
    def send_messages(self, email_messages: List[EmailMessage]):
        succeeded = 0
        for email in email_messages:
            try:
                log.debug('Sending email %s...', email)
                splitted: List[str] = email.to[0].split('@')
                domain = splitted[1].replace('[', '').replace(']', '')

                msg = MIMEMultipart()
                msg['From'] = email.from_email
                msg['To'] = email.to[0]
                msg['Subject'] = email.subject
                msg.attach(MIMEText(email.body, 'plain'))
                with SMTP(domain, port=25, timeout=3) as smtp:
                    smtp.sendmail(email.from_email, email.to[0], msg.as_string())
                succeeded += 1
            except Exception:
                log.info("Failed to send email", exc_info=True)
                raise
        return succeeded
