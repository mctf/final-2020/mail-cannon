import email
import logging
from email import policy
from functools import lru_cache

from django.core import mail
from django.db.models import Q
from django.template import Template, Context
from rest_framework import mixins, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from core.models import Message, User
from core.serializers import MessageSerializer, UserSerializer, MessageBodyRequestSerializer
from mail_cannon import settings

log = logging.getLogger('mail-cannon.core.views')


@lru_cache
def get_tpl_by_str(path):
    with open(settings.BASE_DIR.joinpath('core/templates').joinpath(path), 'rt') as f:
        return f.read()


class MessageView(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    permission_classes = [IsAuthenticated, ]
    serializer_class = MessageSerializer

    def get_queryset(self):
        if self.request.user.is_admin:
            return Message.objects.all()
        else:
            folder = self.request.query_params.get('folder', 'inbound')
            if folder == 'inbound':
                return Message.objects.filter(recipient=self.request.user)
            elif folder == 'outgoing':
                return Message.objects.filter(sender=self.request.user)
            else:
                return Message.objects.filter(Q(recipient=self.request.user) |
                                              Q(sender=self.request.user))

    def create(self, request: Request, *args, **kwargs):
        data = request.data
        try:
            with mail.get_connection() as connection:
                message = mail.EmailMessage(
                    data['subject'], data['text'],
                    request.user.get_email(), [data['recipient_username'], ],
                    connection=connection
                )
                str_repr = str(message.message())
                message.send()
                request.data['text'] = str_repr
                request.data['sender_username'] = str(request.user)
        except Exception as e:
            return Response({
                'description': str(e)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        request.data.pop('subject')
        return super().create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        for message in queryset:
            # noinspection PyBroadException
            try:
                parsed_message = email.message_from_string(message.text, policy=policy.default)
                message.subject = parsed_message.get('Subject', '<NO SUBJECT>')

                message.text = '<NO BODY FOUND>'
                # noinspection PyUnresolvedReferences
                body = parsed_message.get_body(preferencelist=('html', 'plain'))
                if body is not None:
                    message.text = body.get_content()
                    if not message.is_inbound:
                        message.text = message.text.encode('utf-8').decode('unicode-escape')
            except Exception:
                message.subject = '<PARSE ERROR>'
                log.debug('Failed to parse email %s', repr(message), exc_info=True)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UserView(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = [AllowAny, ]
    serializer_class = UserSerializer
    queryset = User.objects.all()


@api_view(['GET', ])
@permission_classes([IsAuthenticated, ])
def get_me(request: Request):
    user = request.user
    serializer = UserSerializer(user)
    return Response({
        'data': serializer.data,
        'user_repr': str(user),
    })


@api_view(['POST', ])
@permission_classes([IsAuthenticated, ])
def get_message_body(request: Request):
    parsed = MessageBodyRequestSerializer(data=request.data)
    parsed.is_valid(raise_exception=True)
    message_pk = parsed.validated_data['pk']
    instance: Message = get_object_or_404(Message.objects, pk=message_pk)

    parsed_message = email.message_from_string(instance.text, policy=policy.default)
    subject = parsed_message.get('Subject', '<NO SUBJECT>')
    text = '<NO BODY FOUND>'
    # noinspection PyUnresolvedReferences
    body = parsed_message.get_body(preferencelist=('html', 'plain'))
    if body is not None:
        text = body.get_content()
        if not instance.is_inbound:
            text = text.encode('utf-8').decode('unicode-escape')

    instance.subject = subject
    instance.text = text

    if instance.is_inbound:  # no recipient_username!
        instance.recipient_username = str(instance.recipient)
    else:
        instance.sender_username = str(instance.sender)

    template = Template(get_tpl_by_str('core/message_frame.html') % instance.sender_username)
    return Response({
        'data': template.render(Context({
            'message': instance,
        }))
    })
