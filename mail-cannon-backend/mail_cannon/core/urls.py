from django.urls import path, include
from rest_framework.authtoken import views as token_views
from rest_framework.routers import DefaultRouter

from core import views
from core.views import MessageView, UserView
from mail_cannon.settings import DEBUG

router = DefaultRouter()
router.register('messages', MessageView, basename='messages')
router.register('register', UserView, basename='register')

urlpatterns = [
    path('', include(router.urls)),
    path('login/', token_views.obtain_auth_token, name='login'),
    path('message/', views.get_message_body, name='get-message-body'),
    path('me/', views.get_me, name='get-me'),
]

if DEBUG:
    urlpatterns += [
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
    ]
