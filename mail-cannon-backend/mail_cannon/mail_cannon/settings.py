"""
Django settings for mail_cannon project.

Generated by 'django-admin startproject' using Django 3.1.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""
import logging
import logging.config
import os
from pathlib import Path

import psutil
from django.core.exceptions import ImproperlyConfigured

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY', default=None)
if SECRET_KEY is None:
    raise ImproperlyConfigured('SECRET_KEY environment variable is not set')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = int(os.environ.get('DEBUG', default=0))

ALLOWED_HOSTS = ['*', ]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'rest_framework.authtoken',
    'core',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
]

if DEBUG:
    MIDDLEWARE += [
        'qinspect.middleware.QueryInspectMiddleware',
    ]

# https://github.com/dobarkod/django-queryinspect
# Whether the Query Inspector should do anything (default: False)
QUERY_INSPECT_ENABLED = True
# Whether to log the stats via Django logging (default: True)
QUERY_INSPECT_LOG_STATS = True
# Whether to add stats headers (default: True)
QUERY_INSPECT_HEADER_STATS = True
# Whether to log duplicate queries (default: False)
QUERY_INSPECT_LOG_QUERIES = True
# Whether to log queries that are above an absolute limit (default: None - disabled)
QUERY_INSPECT_ABSOLUTE_LIMIT = 100  # in milliseconds
# Whether to include tracebacks in the logs (default: False)
QUERY_INSPECT_LOG_TRACEBACKS = True
QUERY_INSPECT_DUPLICATE_MIN = 2  # 0 to force logging of all queries

ROOT_URLCONF = 'mail_cannon.urls'

APPEND_SLASH = False

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAdminUser',
    ],
    'URL_FORMAT_OVERRIDE': None,
    'NUM_PROXIES': 1,
    'DEFAULT_METADATA_CLASS': None,
}

if DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'].append('rest_framework.renderers.BrowsableAPIRenderer')
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] += [
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication'
    ]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'string_if_invalid': 'INVALID_VARIABLE_ERROR' if DEBUG else '',
        },
    },
]

WSGI_APPLICATION = 'mail_cannon.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('SQL_ENGINE', 'django.db.backends.postgresql'),
        'NAME': os.environ.get('SQL_DB', 'mail-cannon'),
        'USER': os.environ.get('SQL_USER', 'mail-cannon'),
        'PASSWORD': os.environ.get('SQL_PASSWORD', 'mail-cannon'),
        'HOST': os.environ.get('SQL_HOST', 'localhost'),
        'PORT': os.environ.get('SQL_PORT', '5432'),
        'CONN_MAX_AGE': 120,
    }
}

AUTH_USER_MODEL = 'core.User'

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = False

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/frontend/'

# Logging
# https://docs.djangoproject.com/en/3.0/topics/logging/

if DEBUG:
    LOG_ROOT = os.path.join(BASE_DIR.parent, '.logs')
else:
    LOG_ROOT = '/logs/'

LOGGING_CONFIG = None
pid = os.getpid()
name = psutil.Process(pid).name()
LOGGING_ALL_FILEPATH = os.path.join(LOG_ROOT, f'{name}_p{pid}_all.log')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console_basic': {
            'format': '%(asctime)s [%(levelname)s] P%(process)d <%(filename)s:%(lineno)d'
                      ', %(funcName)s()> %(name)s: %(message)s',
        },
        'file_text': {
            'format': '%(asctime)s (+%(relativeCreated)d) [%(levelname)s] P%(process)d T%(thread)d'
                      ' <%(pathname)s:%(lineno)d, %(funcName)s at %(module)s> \'%(name)s\': %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'console_basic',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOGGING_ALL_FILEPATH,
            'mode': 'wt',
            'encoding': 'utf-8',
            'formatter': 'file_text',
        },
    },
    'loggers': {
        '': {
            'level': 'INFO',
            'handlers': ['console', 'file', ],
        },
        'mail-cannon': {
            'level': 'DEBUG',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
        'django': {
            'level': 'DEBUG',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
        'django.utils.autoreload': {
            'level': 'INFO',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
        'django.template': {
            'level': 'INFO',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
        'django.db.backends': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
        'http.client': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'handlers': ['console', 'file', ],
            'propagate': False,
        },
    },
}

logging.config.dictConfig(LOGGING)

# Our ip on default interface
# https://stackoverflow.com/a/28950776/10018051
from socket import socket, AF_INET, SOCK_DGRAM


def get_ip():
    s = socket(AF_INET, SOCK_DGRAM)
    # noinspection PyBroadException
    try:
        # doesn't even have recipient be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


DEFAULT_IP = get_ip()

EMAIL_BACKEND = 'core.mailing.VolatileEmailBackend'
