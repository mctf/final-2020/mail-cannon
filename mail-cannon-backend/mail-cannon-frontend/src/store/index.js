import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const persistent = {
	namespaced: true,
	state: {
		authToken: undefined,
		me: null,
	},
	mutations: {
		setAuthToken: (state, n) => state.authToken = n,
		setMe: (state, n) => state.me = n,
		deleteCreds: (state) => {
			state.authToken = undefined;
			state.me = null;
		},
	},
	getters: {
		isLoggedIn: state => {
			return state.authToken && state.me?.username;
		},
	},
};

const volatile = {
	namespaced: true,
	state: {
		messages: [],
	},
	mutations: {
		setMessages: (state, n) => state.messages = n,
	},
};

export default new Vuex.Store({
	modules: {
		persistent,
		volatile,
	},
	plugins: [createPersistedState({
		paths: ['persistent',],
	}),],
});
