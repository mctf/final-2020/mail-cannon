import Axios from 'axios';
import PortalVue from 'portal-vue';
import BootstrapVue from 'bootstrap-vue';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'fontsource-open-sans';
import 'fontsource-open-sans/600.css';
import 'fontsource-jetbrains-mono';

const axiosInstance = Axios.create({
	baseURL: '/api',
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
	},
	timeout: 10000,
});

if (store.state.persistent.authToken) {
	axiosInstance.defaults.headers['Authorization'] = `Token ${store.state.persistent.authToken}`;
}

// noinspection JSUnusedGlobalSymbols
Vue.prototype.$http = axiosInstance;

Vue.use(PortalVue);
Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app');
