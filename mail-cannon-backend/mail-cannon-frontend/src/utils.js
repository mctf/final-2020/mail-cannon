/* eslint-disable no-prototype-builtins */
export function errorJsonToToast(vm, e) {
	if (!e?.response?.data) {
		return e.toString();
	}
	const obj = e.response.data;
	const h = vm.$createElement;
	let resultArr = [];

	if (typeof obj === 'string') {
		resultArr.push(obj);
	} else if (obj.hasOwnProperty('non_field_errors')) {
		obj.non_field_errors.forEach(o => {
			resultArr.push(o.toString(), h('br'));
		});
	} else if (obj.hasOwnProperty('detail')) {
		resultArr.push(obj.detail.toString());
	} else {
		for (let [key, value,] of Object.entries(obj)) {
			resultArr.push(
				'Field ',
				h('b', {}, key),
				': ',
				h('b', {}, value),
				h('br')
			);
		}
	}
	return h('p',
		{class: ['mb-0',],},
		resultArr);
}

export function stringToDatetimeString(string) {
	const datetime = new Date(string);
	return datetime.toLocaleDateString('lt-LT', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		second: '2-digit',
	});
}
