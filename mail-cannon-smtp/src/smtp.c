#include "smtp.h"

void process_conversation(PGconn* db_connection, int socket_fd, char* client_ip, char* server_ip) {
	char read_buffer[BUFF_SIZE];
	char write_buffer[BUFF_SIZE];
	int buffer_offset = 0;
	ssize_t received_bytes;
	bool data_segment_started = false;

	char* verb = calloc(5, 1); // HELO\0

	char* from_field = NULL;
	size_t from_field_len = 0;
	char* to_field = NULL;
	size_t to_field_len = 0;
	size_t new_data_len;
	char body[2048];
	int cur_offset = 0;

	char* db_to_pk = NULL;

	snprintf(write_buffer, BUFF_SIZE, RESP_SERVER_GREETING, server_ip);
	if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
		log_warn("%s: failed to send() greeting: %s", client_ip, strerror(errno));
		close(socket_fd);
		return;
	}

	while (true) {
		fd_set sockset;
		struct timeval tv;

		FD_ZERO(&sockset);
		FD_SET(socket_fd, &sockset);
		tv.tv_sec = 120;
		tv.tv_usec = 0;

		if (select(socket_fd + 1, &sockset, NULL, NULL, &tv) == -1) {
			log_error("%s: select() failed: %s", client_ip, strerror(errno));
			break;
		}

		if (!FD_ISSET(socket_fd, &sockset)) {
			log_info("%s timed out, closing connection", client_ip);
			break;
		}

		// New message arrived!
		log_trace("%s: new message!", client_ip);

		int buffer_left = BUFF_SIZE - buffer_offset - 1;
		if (buffer_left == 0) {
			log_debug("%s: command too long", client_ip);
			snprintf(write_buffer, BUFF_SIZE, RESP_COMMAND_TOO_LONG);
			if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
				log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
				break;
			}
			buffer_offset = 0;
			continue;
		}

		received_bytes = recv(socket_fd, read_buffer + buffer_offset, buffer_left, 0);
		if (received_bytes == 0) {
			log_debug("%s: connection closed", client_ip);
			break;
		}
		if (received_bytes == -1) {
			log_warn("%s: failed to recv(): %s", client_ip, strerror(errno));
			break;
		}
		buffer_offset += received_bytes;

		char* eol;

		// Process multiple lines returned from recv() line-by-line
process_line:
		eol = strstr(read_buffer, "\r\n");
		if (eol == NULL) {
			log_trace("%s: partial content, waiting...", client_ip);
			continue;
		}

		eol[0] = '\0';

		if (data_segment_started) {
			new_data_len = strnlen(read_buffer, BUFF_SIZE);
			if (STREQ(read_buffer, ".")) {
				data_segment_started = false;

				// Save email!
				if (from_field == NULL || to_field == NULL || db_to_pk == NULL) {
					snprintf(write_buffer, BUFF_SIZE, RESP_NOT_ENOUGH_DATA);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}
				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}

				char query[] = "INSERT INTO \"core_message\" (\"created_at\", \"modified_at\", \"is_inbound\", \"recipient_id\", \"recipient_username\",\n\"sender_id\", \"sender_username\", \"text\")\nVALUES (now()::timestamptz, now()::timestamptz, true, %s, '',\nNULL, '%s', '%s');";
				size_t sum_of_lens = strlen(db_to_pk) + strlen(from_field) + strlen(body);
				char* prepared = calloc(sizeof(query) + sum_of_lens, 1);
				snprintf(prepared, sizeof(query) + sum_of_lens, query, db_to_pk, from_field, body);
				PGresult* db_res = PQexec(db_connection, prepared);

				if (PQresultStatus(db_res) != PGRES_COMMAND_OK) {
					log_warn("%s: DB error: %s", client_ip, PQresultErrorMessage(db_res));
					PQclear(db_res);
					snprintf(write_buffer, BUFF_SIZE, RESP_SERVER_ERROR);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
				}
			} else { // Actual data
				strcpy(body + cur_offset, read_buffer);
				body[cur_offset + new_data_len] = '\n';
				cur_offset += new_data_len + 1;
			}
		} else {
			// Capitalize
			for (int i = 0; i < 4; i++) {
				if (islower(read_buffer[i])) {
					read_buffer[i] += 'A' - 'a';
				}
			}
			strncpy(verb, read_buffer, 4); // HELO (\0 already at verb[4])

			char* data = read_buffer;
			data += 5; // Skip verb and space

			/// Big Verb Switch!
			if (STREQ(verb, REQ_HELLO)) {
				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
			} else if (STREQ(verb, REQ_MAIL)) {
				char* tmp = data + 6; // Skip first insignificant symbols (FROM:<admin@[192.168.1.2]>)
				from_field_len = strnlen(tmp, MAX_EMAIL_LEN);
				if (from_field_len <= 0) {
					snprintf(write_buffer, BUFF_SIZE, RESP_INVALID_ARGUMENTS);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}
				from_field = calloc(from_field_len, 1); // Last char will be '\0'
				strncpy(from_field, tmp, from_field_len - 1); // Cut last original char '>'

				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
			} else if (STREQ(verb, REQ_RECIPIENT)) {
				char* tmp = data + 4; // Same here (TO:<user@[192.168.1.2]>)
				to_field_len = strnlen(tmp, MAX_EMAIL_LEN);
				if (to_field_len <= 0) {
					snprintf(write_buffer, BUFF_SIZE, RESP_INVALID_ARGUMENTS);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}
				to_field = calloc(to_field_len, 1);
				strncpy(to_field, tmp, to_field_len - 1);

				/// Separate domain from email
				char* saveptr;
				const char* user = strtok_r(to_field, "@", &saveptr);
				char* domain = strtok_r(NULL, "@", &saveptr);

				domain[strlen(domain) - 1] = '\0';
				if (strncmp(server_ip, domain + 1, strlen(domain) - 1) != 0) {
					snprintf(write_buffer, BUFF_SIZE, RESP_INVALID_DOMAIN, user, domain);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}


				PGresult* db_res = PQexecParams(db_connection,
												"SELECT \"core_user\".\"id\" AS id\nFROM \"core_user\"\nWHERE \"core_user\".\"username\" = $1::VARCHAR\nLIMIT 1;",
												1, NULL, &user, NULL, NULL, 0);
				if (PQresultStatus(db_res) != PGRES_TUPLES_OK) {
					log_warn("%s: DB error: %s", client_ip, PQresultErrorMessage(db_res));
					PQclear(db_res);
					snprintf(write_buffer, BUFF_SIZE, RESP_SERVER_ERROR);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}

				if (PQntuples(db_res) == 0) {
					PQclear(db_res);
					snprintf(write_buffer, BUFF_SIZE, RESP_USER_NOT_FOUND);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}

				const int len = PQgetlength(db_res, 0, 0);
				db_to_pk = calloc(len, 1);
				strncpy(db_to_pk, PQgetvalue(db_res, 0, 0), len);

				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
			} else if (STREQ(verb, REQ_DATA)) {
				snprintf(write_buffer, BUFF_SIZE, RESP_CONTINUE);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
				data_segment_started = true;
			} else if (STREQ(verb, REQ_RESET)) {
				free(to_field);
				to_field = NULL;
				to_field_len = 0;
				free(from_field);
				from_field = NULL;
				from_field_len = 0;
				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
			} else if (STREQ(verb, REQ_NOOP)) {
				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
			} else if (STREQ(verb, REQ_DEBG)) {
				FILE* map = fopen("/proc/self/maps", "r");
				if (map == NULL) {
					snprintf(write_buffer, BUFF_SIZE, RESP_SERVER_ERROR);
					if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
						log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
						break;
					}
					goto continue_loop;
				}

				char* buf = NULL;
				size_t buf_len = 0;
				char* line = NULL;
				size_t len = 0;
				ssize_t read;

				while ((read = getline(&line, &len, map)) != -1) {
					if (buf == NULL) {
						buf = calloc(read, 1);
						buf_len = read;
						memcpy(buf, line, read - 1);
						buf[read - 1] = '\t';
					} else {
						buf = realloc(buf, buf_len + read);
						memcpy(buf + buf_len, line, read - 1);
						buf_len += read;
						buf[buf_len - 1] = '\t';
					}
				}

				buf[buf_len - 1] = '\0';
				fclose(map);

				snprintf(write_buffer, BUFF_SIZE, RESP_OKAY_DEBG, buf);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					free(buf);
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
					break;
				}
				free(buf);
			} else if (STREQ(verb, REQ_QUIT)) {
				log_debug("%s: requested QUIT, closing connection", client_ip);
				snprintf(write_buffer, BUFF_SIZE, RESP_BYE, server_ip);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
				}
				break; // Exit!
			} else {
				log_debug("%s: got unknown verb %s", client_ip, verb);
				snprintf(write_buffer, BUFF_SIZE, RESP_COMMAND_NOT_IMPLEMENTED);
				if (send(socket_fd, write_buffer, strnlen(write_buffer, BUFF_SIZE), MSG_NOSIGNAL) == -1) {
					log_warn("%s: failed to send(): %s", client_ip, strerror(errno));
				}
			}
			///
		}

continue_loop:
		// Shift the rest of the buffer to the front
		memmove(read_buffer, eol + 2, BUFF_SIZE - (eol + 2 - read_buffer));
		buffer_offset -= (eol - read_buffer) + 2;

		// Do we already have additional lines to process? If so,
		// commit a horrid sin and goto the line processing section again.
		if (strstr(read_buffer, "\r\n"))
			goto process_line;

	}
	free(to_field);
	free(from_field);
	free(verb);
//	free(body);
	close(socket_fd);
}
