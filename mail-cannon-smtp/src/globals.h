#ifndef MAIL_CANNON_SMTP_GLOBALS_H
#define MAIL_CANNON_SMTP_GLOBALS_H

#define EXIT_FAILURE_ARGS 1
#define EXIT_FAILURE_DB_CONNECTION 2
#define EXIT_FAILURE_DB_TEST 4
#define EXIT_FAILURE_ADDRINFO 8
#define EXIT_FAILURE_SOCKET 16
#define EXIT_FAILURE_SOCKOPT 32
#define EXIT_FAILURE_BIND 64
#define EXIT_FAILURE_LISTEN 128

#define SMTP_PORT "25"

static const char default_db_connection_string[] =
		"host=localhost user=mail-cannon password=mail-cannon"
		" dbname=mail-cannon connect_timeout=30 application_name=mail-cannon-smtp"
		" sslmode=disable";

#endif //MAIL_CANNON_SMTP_GLOBALS_H
