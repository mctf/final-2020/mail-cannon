#include "main.h"

int main(int argc, char* argv[]) {
	char* db_connection_string = calloc(4096, 1);
	strncpy(db_connection_string, default_db_connection_string, 4096);

	long n_threads = 2;
	pthread_mutex_init(&log_mutex, NULL);
	log_set_lock(lock_callback, &log_mutex);
	log_set_level(LOG_INFO);
	log_info("Logger initialized");

	int v_times = 0;
	int c;
	while (true) {
		int option_index = 0;
		c = getopt_long(argc, argv, short_options, long_options, &option_index);
		if (c == -1) break;

		switch (c) {
			case 'v': {
				if (v_times == 0) {
					log_set_level(LOG_DEBUG);
					log_debug("Log level is set to DEBUG");
					++v_times;
				} else if (v_times == 1) {
					log_set_level(LOG_TRACE);
					log_trace("Log level is set to TRACE");
					++v_times;
				} else {
					log_warn("Cannot be more verbose!");
				}
				break;
			}
			case 'd': {
				strncpy(db_connection_string, optarg, 4096);
				break;
			}
			case 't': {
				if (str2long(&n_threads, optarg, 10) != STR2INT_SUCCESS) {
					log_fatal("Failed to parse number of threads");
					return EXIT_FAILURE_ARGS;
				}
				if (n_threads < 1 || n_threads > 69420) {
					log_fatal("Incorrect number of threads specified");
					return EXIT_FAILURE_ARGS;
				}
				break;
			}
			default:
				log_fatal("Failed to parse args! #TODO: usage");
				free(db_connection_string);
				return EXIT_FAILURE_ARGS;
		}
	}

	log_debug("Connecting to '%s' via libpq v%d (thread-safe? %d)",
			  db_connection_string, PQlibVersion(), PQisthreadsafe());
	PGconn* db_connection = PQconnectdb(db_connection_string);
	if (PQstatus(db_connection) != CONNECTION_OK) {
		log_fatal("Database connection failed: %s", PQerrorMessage(db_connection));
		PQfinish(db_connection);
		free(db_connection_string);
		return EXIT_FAILURE_DB_CONNECTION;
	}

	log_debug("Successfully connected, testing connection");
	PGresult* test_results = PQexecParams(db_connection, "SELECT 1::INTEGER AS \"test\";", 0,
										  NULL, NULL, NULL, NULL, 1);
	if (PQresultStatus(test_results) != PGRES_TUPLES_OK
		|| PQnfields(test_results) != 1
		|| PQfnumber(test_results, "test") != 0) {
		log_fatal("Database test failed: %s", PQresultErrorMessage(test_results));
		PQclear(test_results);
		PQfinish(db_connection);
		free(db_connection_string);
		return EXIT_FAILURE_DB_TEST;
	}
	PQclear(test_results);
	log_debug("Connection is alive");
	PQfinish(db_connection);

	/// Init server!
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;

	struct addrinfo* hostinfo = NULL;
	int error = getaddrinfo(NULL, SMTP_PORT, &hints, &hostinfo);
	if (error) {
		log_fatal("Failed to getaddrinfo(): %s", gai_strerror(error));
		free(db_connection_string);
		return EXIT_FAILURE_ADDRINFO;
	}
	log_trace("getaddrinfo() succeeded");

	int server_fd = socket(hostinfo->ai_family, hostinfo->ai_socktype, hostinfo->ai_protocol);
	if (server_fd == -1) {
		log_fatal("Failed to socket(): %s", strerror(errno));
		freeaddrinfo(hostinfo);
		free(db_connection_string);
		return EXIT_FAILURE_SOCKET;
	}
	log_trace("socket() succeeded");

	int yes = 1;
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
		log_fatal("Failed to setsockopt(): %s", strerror(errno));
		freeaddrinfo(hostinfo);
		free(db_connection_string);
		return EXIT_FAILURE_SOCKOPT;
	}
	log_trace("setsockopt() succeeded");

	if (bind(server_fd, hostinfo->ai_addr, hostinfo->ai_addrlen) == -1) {
		log_fatal("Failed to bind(): %s", strerror(errno));
		freeaddrinfo(hostinfo);
		close(server_fd);
		free(db_connection_string);
		return EXIT_FAILURE_BIND;
	}
	log_trace("bind() succeeded");
	freeaddrinfo(hostinfo);

	if (listen(server_fd, SOMAXCONN) == -1) {
		log_fatal("Failed to listen(): %s", strerror(errno));
		close(server_fd);
		free(db_connection_string);
		return EXIT_FAILURE_LISTEN;
	}
	log_trace("listen() succeeded");
	///


	log_info("Preforking %ld thread(s)", n_threads);
	pthread_t* threads = malloc(n_threads * sizeof(pthread_t));
	process_tcp_thread_arg_t thread_arg = {
			.socket_fd = server_fd,
			.db_connection_string = db_connection_string,
	};
	for (int i = 0; i < n_threads; ++i) {
		pthread_create(&threads[i], NULL, &process_tcp_thread, &thread_arg);
	}

	for (int i = 0; i < n_threads; ++i) {
		pthread_join(threads[i], NULL);
		log_trace("Thread %d finished!", i);
	}

	free(threads);
	close(server_fd);
	free(db_connection_string);
	return EXIT_SUCCESS;
}

void* process_tcp_thread(void* raw_thread_arg) {
	pid_t curr_tid = gettid();
	log_trace("Started thread %d", curr_tid);

	process_tcp_thread_arg_t* thread_arg = raw_thread_arg;
	int socket_fd = thread_arg->socket_fd;
	char* db_connection_string = calloc(4096, 1);
	strncpy(db_connection_string, thread_arg->db_connection_string, 4096);

	PGconn* db_connection = PQconnectdb(db_connection_string);
	if (PQstatus(db_connection) != CONNECTION_OK) {
		log_fatal("Database connection failed: %s", PQerrorMessage(db_connection));
		PQfinish(db_connection);
		free(db_connection_string);
		exit(EXIT_FAILURE_DB_CONNECTION);
	}

	struct sockaddr_storage server_sockinfo;
	uint32_t server_sockinfo_len = sizeof(server_sockinfo);

	struct sockaddr_storage client_sockinfo;
	socklen_t client_sockinfo_len = sizeof(client_sockinfo);

	char client_ip_buff[INET6_ADDRSTRLEN];
	char server_ip_buff[INET6_ADDRSTRLEN];
	while (true) {
		int session_fd = accept(socket_fd,
								(struct sockaddr*) &client_sockinfo, &client_sockinfo_len);
		if (session_fd == -1) {
			if (errno == EINTR) {
				log_warn("Interrupted!");
				continue;
			}
			log_error("Failed to accept(): %s", strerror(errno));
			continue;
		}
		if (getsockname(session_fd, (struct sockaddr*) &server_sockinfo, &server_sockinfo_len) != 0) {
			log_error("Failed to getsockname(): %s", strerror(errno));
			continue;
		}

		void* server_ip = get_in_addr((struct sockaddr*) &server_sockinfo);
		inet_ntop(server_sockinfo.ss_family, server_ip, server_ip_buff, sizeof(server_ip_buff));

		void* client_ip = get_in_addr((struct sockaddr*) &client_sockinfo);
		inet_ntop(client_sockinfo.ss_family, client_ip, client_ip_buff, sizeof(client_ip_buff));
		log_debug("Received new connection from %s on iface w/ IP %s", client_ip_buff, server_ip_buff);

		process_conversation(db_connection, session_fd, client_ip_buff, server_ip_buff);
	}

	PQfinish(db_connection);
	free(db_connection_string);
	return NULL;
}

static void lock_callback(bool lock, void* udata) {
	if (lock) {
		pthread_mutex_lock(udata);
	} else {
		pthread_mutex_unlock(udata);
	}
}
