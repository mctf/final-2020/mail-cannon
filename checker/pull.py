import sys

from redis import StrictRedis

from mail_cannon import pull_flag

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
ip = sys.argv[3]
oldFlag = sys.argv[4]

redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)

username = redis.get(f'checkers_state/mail-cannon/{ip}/username')
password = redis.get(f'checkers_state/mail-cannon/{ip}/password')

status, trace, stderr = pull_flag(ip, username, password, oldFlag)
print(status)
print(trace, flush=True)
print(stderr, file=sys.stderr, flush=True)
