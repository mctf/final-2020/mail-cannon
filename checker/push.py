import sys

from redis import StrictRedis

from mail_cannon import push_flag

redisHost = sys.argv[1]
redisPassword = sys.argv[2]
ip = sys.argv[3]
newFlag = sys.argv[4]

redis = StrictRedis(host=redisHost, port=6379, password=redisPassword, decode_responses=True)

username, password, status, trace, stderr = push_flag(ip, newFlag)

if status == 'UP':
    redis.set(f'checkers_state/mail-cannon/{ip}/username', username)
    redis.set(f'checkers_state/mail-cannon/{ip}/password', password)

print(status)
print(trace, flush=True)
print(stderr, file=sys.stderr, flush=True)
